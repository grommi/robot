﻿namespace Robot
{
    interface IRobotAI
    {
        /// <summary>
        /// Генерирует команду для робота
        /// </summary>
        /// <param name="robot"> Робот, которым управляем</param>
        /// <param name="finish"> Пункт назначения</param>
        /// <param name="roundBarriers"> Круглые препядствия</param>
        /// <param name="polygonalBarriers"> Сложные полигональные препядствия</param>
        /// <returns>Команда, по которой будет пытаться двигаться робот</returns>
        Command GenerateCommand(IRobot robot, Point finish, ISimpleRoundWorldObject[] roundBarriers, IPolygonalWorldObject[] polygonalBarriers);
    }
}
