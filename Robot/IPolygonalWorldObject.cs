﻿namespace Robot
{
    interface IPolygonalWorldObject
    {
        /// <summary>
        /// Массив точек образующих многоугольник, направление обхода - против часовой стрелки
        /// </summary>
        /// <returns></returns>
        Point[] GetPolygon();
    }
}
