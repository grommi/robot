﻿using System.Drawing;

namespace Robot
{
    interface ICanBeDrawn
    {
        void Draw(Graphics g);
    }
}
