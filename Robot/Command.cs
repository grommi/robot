﻿namespace Robot
{
    class Command
    {
        /// <summary>
        /// Скорость, с которой будет двигатся робот (если может) в пикселях в секунду
        /// </summary>
        public double Velocity { get; }
        /// <summary>
        /// Угловая скорость (Т.е. скорость поворота робота) с которой будет пытаться двигаться робот (если сможет) в радианах в секунду
        /// </summary>
        public double AngularVelocity { get; }
        /// <summary>
        /// Время, которое робот будет двигаться в миллисекундах
        /// </summary>
        public double Time { get; }
        /// <summary>
        /// Создает новую команду роботу
        /// </summary>
        /// <param name="velocity">Скорость, с которой будет двигатся робот (если может) в пикселях в секунду</param>
        /// <param name="angularVelocity">Угловая скорость (Т.е. скорость поворота робота) с которой будет пытаться двигаться робот (если сможет) в радианах в секунду</param>
        /// <param name="time">Время, которое робот будет двигаться, в миллисекундах</param>
        public Command(double velocity, double angularVelocity, double time)
        {
            Velocity = velocity;
            AngularVelocity = angularVelocity;
            Time = time;
        }
    }
}
