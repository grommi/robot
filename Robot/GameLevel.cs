﻿using System.Drawing;

namespace Robot
{
    class GameLevel
    {
        public ISimpleRoundWorldObject[] RoundBarriers { get; }
        public IPolygonalWorldObject[] PolygonalBarriers { get; }
        public IRobot Robot { get; }
        private Pen finishPen = new Pen(Setting.World.FinishColor);
        public Point Start { get; }
        public Point Finish { get; }
        public double StartDirection { get; }

        /// <summary>
        /// Уровень игры
        /// </summary>
        /// <param name="roundBarriers">Массив круглых стен</param>
        /// <param name="polygonalBarriers">Массив сложных полигональных стен</param>
        /// <param name="start">Стартовое положение</param>
        /// <param name="finish">Пункт назначения</param>
        /// <param name="startDirection">Начальное направление движения робота, угол между направлением движения робота и положительным направлением оси Х</param>
        /// <param name="size">Размер робота, т.е. его радиус в пикселях</param>
        /// <param name="maxVelocity">Максимальная скорость робота в пикселях в секунду</param>
        /// <param name="maxAngularVelocity">Максимальная скорость поворота робота в радианах в секунду</param>
        /// <param name="ai">ИИ робота(Место для вашей рекламы)</param>
        public GameLevel(
            ISimpleRoundWorldObject[] roundBarriers,
            IPolygonalWorldObject[] polygonalBarriers,
            Point start,
            Point finish,
            double startDirection,
            double size,
            double maxVelocity,
            double maxAngularVelocity,
            IRobotAI ai)
        {
            RoundBarriers = roundBarriers;
            PolygonalBarriers = polygonalBarriers;
            Start = start;
            Finish = finish;
            StartDirection = startDirection;
            var robot = new Robot(start, startDirection, size, maxVelocity, maxAngularVelocity, ai)
            {
                VisiblePolygonBarriers = polygonalBarriers,
                VisibleRoundBarriers = roundBarriers,
                Finish = finish
            };
            Robot = robot;
        }

        public void DrawFinish(Graphics g)
        {
            var fin = Finish;
            var fx = (int)fin.X;
            var fy = (int)fin.Y;
            g.DrawLine(finishPen, new System.Drawing.Point(fx - 5, fy - 5), new System.Drawing.Point(fx + 5, fy + 5));
            g.DrawLine(finishPen, new System.Drawing.Point(fx - 5, fy + 5), new System.Drawing.Point(fx + 5, fy - 5));
        }
    }
}
