﻿namespace Robot
{
    interface IRobot : ISimpleRoundWorldObject, INeedReDraw
    {
        /// <summary>
        /// Возвращает текущее направление движения робота
        /// </summary>
        /// <returns>Угол между вектором движения робота и положительным направлением оси ОХ</returns>
        double GetDirection();
        /// <summary>
        /// Возвращает максимальную скорость робота(P.S. Минимальная 0)
        /// </summary>
        /// <returns>Максимальная скорость робота в пикселях в секунду</returns>
        double GetMaxVelocity();
        /// <summary>
        /// Возвращает максимальную скорость поворота робота 
        /// </summary>
        /// <returns>Максимальная скорость поворота робота, в радианах в секунду</returns>
        double GetMaxAngularVelocity();
    }
}
