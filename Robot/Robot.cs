﻿using System;
using System.Drawing;

namespace Robot
{
    class Robot : IRobot
    {
        private Point Pos { get; set; }
        private Point LastPos { get; set; }
        private double Size { get; }
        private double Direction { get; set; }
        private double MaxVelocity { get; }
        private double MinVelocity { get; }
        private double MaxAngularVelocity { get; }

        public IPolygonalWorldObject[] VisiblePolygonBarriers { get; set; }
        public ISimpleRoundWorldObject[] VisibleRoundBarriers { get; set; }

        private IRobotAI Ai { get; }
        public Point Finish { get; set; }

        private Command Cmd { get; set; }
        private double StartTimeCmd { get; set; }
        private double StartDirCmd { get; set; }
        private Point StartPosCmd { get; set; }


        private Pen Pen { get; }
        private Brush Brush { get; }
        private Brush BackGround { get; }

        public Robot(Point position,double direction, double size, double maxVelocity, double maxAngularVelocity, IRobotAI ai, Color mainColor, Color secondColor)
        {
            Pos = position;
            LastPos = Pos;
            Direction = direction;
            Size = size;
            Ai = ai;
            Pen = new Pen(secondColor);
            Brush = new SolidBrush(mainColor);
            BackGround = new SolidBrush(Setting.World.BackGround);
            MinVelocity = 0;
            MaxAngularVelocity = maxAngularVelocity;
            MaxVelocity = maxVelocity;
        }

        public Robot(Point position,double direction, double size, double maxVelocity, double maxAngularVelocity, IRobotAI ai): 
            this(position,direction, size, maxVelocity, maxAngularVelocity, ai, Setting.Robot.BorderColor, Setting.Robot.FillColor){}
        public void Draw(Graphics g)
        {
            var xc = (int)(Pos.X - Size);
            var yc = (int)(Pos.Y - Size);
            g.FillEllipse(Brush, xc, yc, (int)(Size*2), (int)(Size*2));
            g.DrawEllipse(Pen, xc, yc, (int)(Size*2), (int)(Size*2));
            g.DrawLine(Pen, (int)Pos.X, (int)Pos.Y, (int)(Pos.X+Size*Math.Cos(Direction)), (int)(Pos.Y+Size*Math.Sin(Direction)));
        }

        public Point GetCenter() => Pos;
        public double GetSize() => Size;
        public double GetDirection() => Direction;
        public double GetMaxVelocity() => MaxVelocity;
        public double GetMaxAngularVelocity() => MaxAngularVelocity;

        public bool ReDraw(Graphics g)
        {
            var isFinish = Move(RobotWorld.Time);
            var xc = (int)(LastPos.X - Size)-1;
            var yc = (int)(LastPos.Y - Size)-1;
            g.FillEllipse(BackGround, xc, yc, (int)(Size*2+2), (int)(Size*2+2));
            LastPos = Pos;
            Draw(g);
            return isFinish;
        }

        private Command ProtectedGenerateCommand()
        {
            var cmd = Ai.GenerateCommand(this, Finish, VisibleRoundBarriers, VisiblePolygonBarriers);
            return  new Command(
                Math.Min(Math.Max(cmd.Velocity, MinVelocity), MaxVelocity), 
                Math.Abs(cmd.AngularVelocity)<= MaxAngularVelocity? cmd.AngularVelocity: MaxAngularVelocity * Math.Sign(cmd.AngularVelocity),
                cmd.Time);
        }
        private void SuncTime()
        {
            Cmd = ProtectedGenerateCommand();
            StartTimeCmd = RobotWorld.Time;
            StartDirCmd = Direction;
            StartPosCmd = Pos;
        }
        private bool Move(double time)
        {
            if(Cmd == null) SuncTime();
            var localTime = time - StartTimeCmd;
            if (localTime >= Cmd.Time)
            {
                CircularMove(Cmd.Time/1000);
                Direction = Direction % (Math.PI * 2);
                StartTimeCmd += Cmd.Time;
                StartDirCmd = Direction;
                StartPosCmd = Pos;
                Cmd = ProtectedGenerateCommand();
                return Move(time);
            }
            else
            {
                CircularMove(localTime / 1000);
                return CheckCrash() || Point.GetDistance(Pos, Finish) < Size;
            }
        }

        private bool CheckCrash()
        {
            foreach (var barrier in VisibleRoundBarriers)
            {
                var isCrash = Point.GetDistance(barrier.GetCenter(), Pos) <= Size + barrier.GetSize();
                if (isCrash) return true;
            }
            return false;
        }

        private void CircularMove(double time)
        {
            if (Math.Abs(Cmd.AngularVelocity) < double.Epsilon)
            {
                Pos = new Point(StartPosCmd.X+Cmd.Velocity*time*Math.Cos(StartDirCmd), StartPosCmd.Y+Cmd.Velocity*time*Math.Sin(StartDirCmd));
            }
            else
            {
                var radius = Cmd.Velocity / Cmd.AngularVelocity;
                var angle = Cmd.AngularVelocity * time;
                Pos = CircularMove(StartPosCmd, StartDirCmd, angle, radius);
                Direction = StartDirCmd + angle;
            }
        }

        private static Point CircularMove(Point start, double dimension, double angle, double radius)
        {
            var dx = - radius * Math.Sin(dimension);
            var dy = - radius * -Math.Cos(dimension);
            var finishAngle = Math.Atan2(-dy, -dx) + angle;
            return new Point(
                start.X + dx + Math.Cos(finishAngle)*Math.Abs(radius),
                start.Y + dy + Math.Sin(finishAngle)* Math.Abs(radius));
        }
    }
}
