﻿using System.Drawing;

namespace Robot
{
    class RoundBarrier : ISimpleRoundWorldObject
    {
        private static readonly Pen Pen = new Pen(Setting.Wall.BorderColor);
        private static readonly Brush Brush = new SolidBrush(Setting.Wall.FillColor);

        private Point Center { get; }
        private double Size { get; }

        public RoundBarrier(Point center, double size)
        {
            Center = center;
            Size = size;
        }

        public void Draw(Graphics g)
        {
            var xc = (int)(Center.X - Size);
            var yc = (int)(Center.Y - Size);
            g.FillEllipse(Brush, xc, yc, (int)Size*2, (int)Size*2);
            g.DrawEllipse(Pen, xc, yc, (int)Size*2, (int)Size*2);
        }

        public Point GetCenter() => Center;

        public double GetSize() => Size;
    }
}
