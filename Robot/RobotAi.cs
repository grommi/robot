﻿namespace Robot
{
    class RobotAi : IRobotAI
    {
        public Command GenerateCommand(
            IRobot robot,
            Point finish,
            ISimpleRoundWorldObject[] roundBarriers,
            IPolygonalWorldObject[] polygonalBarriers)
        {
            return CreateCommand(robot, finish, roundBarriers, polygonalBarriers);
        }
        public static Command CreateCommand(
            IRobot robot,
            Point finish,
            ISimpleRoundWorldObject[] roundBarriers,
            IPolygonalWorldObject[] polygonalBarriers)
        {
            //нужно написать этот метод

            //пример команды: робот будет двигаться со скоростью 60 пикселей в секунду,
            //поворачивая со скоростью 1 радиан в секунду вправо, в течение 1000 миллисекунд(т.е. 1 секунды)
            return new Command(60, 1, 1000);
        }
    }
}
