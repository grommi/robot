﻿namespace Robot
{
    interface ISimpleRoundWorldObject : ICanBeDrawn
    {
        /// <summary>
        /// Возвращает координаты центра объекта
        /// </summary>
        /// <returns>Точка, являющаяся центром объекта</returns>
        Point GetCenter();

        /// <summary>
        /// Возвращает размер объекта, то есть радиус круга
        /// </summary>
        /// <returns> Радиус объекта</returns>
        double GetSize();
    }
}
