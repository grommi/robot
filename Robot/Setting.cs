﻿using System.Drawing;

namespace Robot
{
    class Setting
    {
        public static readonly WorldSetting World = new WorldSetting();
        public static readonly WallSetting Wall = new WallSetting();
        public static readonly RobotSetting Robot = new RobotSetting();
    }

    class WorldSetting
    {
        public readonly double Eps = 0.01;
        public readonly int Tick = 25;
        public readonly Color BackGround = Color.PaleGreen;
        public readonly Color FinishColor = Color.Indigo;
    }

    class WallSetting
    {
        public readonly Color FillColor = Color.DimGray;
        public readonly Color BorderColor = Color.Black;
    }

    class RobotSetting
    {
        public readonly Color FillColor = Color.DarkSalmon;
        public readonly Color BorderColor = Color.Maroon;
    }
}
