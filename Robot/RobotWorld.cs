﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Robot
{
    public partial class RobotWorld : Form
    {
        public static int Time { get; set; }

        private GameLevel GameLevel { get; set; }
        private Graphics _g;
        private Bitmap _buffer;
        
        public RobotWorld()
        {
            InitializeComponent();
        }

        public void Start(Object sender, EventArgs args)
        {
            Time = 0;
            _buffer = new Bitmap(Screen.Width, Screen.Height);
            _g = Graphics.FromImage(_buffer);
            MainTimer.Interval = Setting.World.Tick;
            GameLevel = Program.GenerateGameLevel();
            _g.FillRectangle(new SolidBrush(Setting.World.BackGround),0,0, Screen.Width, Screen.Height );
            foreach (var barrier in GameLevel.RoundBarriers)
            {
                barrier.Draw(_g);
            }
            GameLevel.Robot.Draw(_g);
            GameLevel.DrawFinish(_g);
            MainTimer.Enabled = true;
            Screen.Image = _buffer;
        }

        public void ReDraw(Object sender, EventArgs args)
        {
            Time += MainTimer.Interval;
            if (GameLevel.Robot.ReDraw(_g))
            {
                Stop(sender, args);
            }
            GameLevel.DrawFinish(_g);
            Screen.Image = _buffer;
        }

        public void Stop(Object sender, EventArgs args)
        {
            MainTimer.Enabled = false;
            var distance = Point.GetDistance(GameLevel.Robot.GetCenter(), GameLevel.Finish);
            OutPut.Text = distance < Setting.World.Eps ? "Finish" : "Distance to finish: " + distance;
        }
        
    }
}
