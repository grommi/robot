﻿using System;
using System.Windows.Forms;

namespace Robot
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new RobotWorld());
        }

        public static GameLevel GenerateGameLevel()
        {
            //место для вашего уровня
            return new GameLevel(
                new ISimpleRoundWorldObject[]{new RoundBarrier(new Point(700,350), 70 )}, 
                null,
                new Point(500,350),
                new Point(200,200),
                Math.PI/4,
                40,
                100,
                Math.PI/2,
                new RobotAi()
                );
        }
    }
}
