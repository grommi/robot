﻿using System.Drawing;

namespace Robot
{
    interface INeedReDraw
    {
        bool ReDraw(Graphics g);
    }
}
