��� ���������� ����������� ��������� IRobotAI, ��� ����� ��� ������ ����� RobotAI, �� ��� ��������� �������� ����� CreateCommand.

�� ��������� ��������� ���������:
	- IRobot robot: ��� ���������� �����, ������� �� ����������.
	- Point finish: ��� ����� ���� ����� ������ ��������� (��� ����������� ������������) ���������, ��� ����� ��������, ���� �������� ����� ��������� ������ ������.
	- ISimpleRoundWorldObject[] roundBarriers: ������ ������� ������� ������, ��� ������� ������ ����� ��������.
	- IPolygonalWorldObject[] polygonalBarriers: ������ ������� ������������� ������ (�.�. ���������������), ���� �� �����������, �� ��� ������ �������� ��� �� ���������.

��������� � ������ �� ���:
IRobot ����� ��������:
	- ���� ������� �������
	- ���� ������
	- ���� ������� ����������� ��������(�������)
	- ���� ������������ �������� � �������� � �������
	- ���� ������������ �������� �������� � �������� � �������
Point 
	- ������ ����� �� ���������, ����� ���������� x � y, �����������. ����� ����� Point ����� ������� ���������� ����� ����� �������.
ISimpleRoundWorldObject ����� ��������:
	- ���� �������
	- ���� ������
IPolygonalWorldObject ����� ��������:
	- ������ �����, ���������� ������ �������������� ������������� �����������, ����������� ������ - ������������� (�.�. ������ ������� ������� � ����������� ����)



��� ����� ������ ������� �������:
��� ��� �����:
	- ��������, � ������� ������ ��������� ����� (������� � �������)
	- �������� �������� (������� � �������, ������������� �������� ������������ �������� �������, ������������� - ������)
	- ����� �������� � ������������� (1 ������� = 1000 �����������)

������� ������� ����� ����� ���������, ��� ���� �� ��������� ������, ��� ���� �� ���������, ��� ���� �� ���������� ����� ��������.
���� ����� �� ������ ������ � �� �������� �� ������ ��������� �������� - �� �������� � �� ����� �������. 